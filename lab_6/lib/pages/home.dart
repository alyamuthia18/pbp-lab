import 'package:flutter/material.dart';
import 'package:lab_6/main_drawer.dart';
import 'package:lab_6/pages/body.dart';

class HomeLab6 extends StatelessWidget {
  const HomeLab6({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Progress Tracker',
          style: TextStyle(color: Color(0xFFe0e1dd)),
        ),
        backgroundColor: Color(0xFF343434),
      ),
      drawer: MainDrawer(),
      body: MainBody(),
    );
  }
}
