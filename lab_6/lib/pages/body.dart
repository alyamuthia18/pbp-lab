import 'package:flutter/material.dart';

class MainBody extends StatelessWidget {
  TextEditingController taskController = TextEditingController();
  TextEditingController statusController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Color(0xff0d1b2a),
        child: Padding(
            padding: EdgeInsets.all(10),
            child: ListView(children: <Widget>[
              Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.fromLTRB(30, 30, 30, 20),
                  child: Text(
                    'Task',
                    style: TextStyle(
                        fontFamily: 'Segoe UI',
                        color: Color(0xFFe0e1dd),
                        fontSize: 20),
                  )),
              Container(
                  padding: EdgeInsets.fromLTRB(140, 10, 140, 10),
                  child: TextField(
                    controller: taskController,
                    decoration: InputDecoration(
                        filled: true,
                        fillColor: Color(0xFF748ba5),
                        contentPadding: EdgeInsets.all(1.0),
                        border: OutlineInputBorder(),
                        labelText: 'Add Your Task here',
                        labelStyle: TextStyle(
                          fontFamily: 'Segoe UI',
                          color: Color(0xFFadb5bd),
                        )),
                  )),
              Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.fromLTRB(30, 30, 30, 20),
                  child: Text(
                    'Status',
                    style: TextStyle(
                        fontFamily: 'Segoe UI',
                        color: Color(0xFFe0e1dd),
                        fontSize: 20),
                  )),
              Container(
                  padding: EdgeInsets.fromLTRB(140, 10, 140, 10),
                  child: TextField(
                    controller: statusController,
                    decoration: InputDecoration(
                        filled: true,
                        fillColor: Color(0xFF748ba5),
                        contentPadding: EdgeInsets.all(1.0),
                        border: OutlineInputBorder(),
                        labelText: 'Type The Status Here',
                        hintText: 'No Status',
                        labelStyle: TextStyle(
                          fontFamily: 'Segoe UI',
                          color: Color(0xFFadb5bd),
                        )),
                  )),
              Container(
                padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
                child: Center(
                    child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    onPrimary: Colors.white,
                    primary: Color(0xFF748ba5),
                  ),
                  child: Text('Add'),
                  onPressed: () {
                    print(taskController.text);
                    print(statusController.text);
                  },
                )),
              ),
            ])));
  }
}
