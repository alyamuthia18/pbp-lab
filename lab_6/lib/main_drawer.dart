import 'package:flutter/material.dart';

// import '../screens/filters_screen.dart';

class MainDrawer extends StatelessWidget {
  Widget buildListTile(String title, IconData icon, Function tapHandler) {
    return ListTile(
      leading: Icon(
        icon,
        size: 26,
      ),
      title: Text(
        title,
        style: TextStyle(
          fontFamily: 'RobotoCondensed',
          fontSize: 24,
          fontWeight: FontWeight.bold,
        ),
      ),
      // onTap: tapHandler,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            height: 120,
            width: double.infinity,
            padding: EdgeInsets.all(20),
            alignment: Alignment.centerLeft,
            color: Color(0xFF343434),
            child: Text(
              'Kladaf',
              style: TextStyle(
                  fontWeight: FontWeight.w900,
                  fontSize: 30,
                  color: Color(0xFFe0e1dd)),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          buildListTile('Diary', Icons.sticky_note_2, () {
            // Navigator.of(context).pushReplacementNamed('/');
          }),
          buildListTile('Podomoro Timer', Icons.timer, () {
            // Navigator.of(context).pushReplacementNamed(FiltersScreen.routeName);
          }),
          buildListTile('Progress Tracker', Icons.assignment_turned_in, () {
            // Navigator.of(context).pushReplacementNamed('/');
          }),
          buildListTile('Weekly Schedule', Icons.calendar_today, () {
            // Navigator.of(context).pushReplacementNamed('/');
          }),
          buildListTile('Wishlist', Icons.fact_check, () {
            // Navigator.of(context).pushReplacementNamed('/');
          }),
        ],
      ),
    );
  }
}
