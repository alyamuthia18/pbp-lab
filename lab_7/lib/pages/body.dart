import 'package:flutter/material.dart';
import 'package:lab_7/pages/home.dart';

// di tombol add todo panggil homeLab
class LandingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff0d1b2a),
      appBar: AppBar(
        title: Text(
          'Progress Tracker',
          style: TextStyle(color: Color(0xFFe0e1dd)),
        ),
        backgroundColor: Color(0xFF343434),
      ),
      body: Center(
          child: Column(children: <Widget>[
        Container(
          margin: EdgeInsets.all(10),
          child: Table(
              border: TableBorder.all(color: Color(0xFFe0e1dd)),
              children: [
                TableRow(children: [
                  Column(children: [
                    Text(
                      'No Status',
                      style: TextStyle(color: Color(0xFFe0e1dd)),
                    )
                  ]),
                  Column(children: [
                    Text(
                      'Not Started',
                      style: TextStyle(color: Color(0xFFe0e1dd)),
                    )
                  ]),
                  Column(children: [
                    Text(
                      'In Progress',
                      style: TextStyle(color: Color(0xFFe0e1dd)),
                    )
                  ]),
                  Column(children: [
                    Text(
                      'In Review',
                      style: TextStyle(color: Color(0xFFe0e1dd)),
                    )
                  ]),
                  Column(children: [
                    Text(
                      'Completed',
                      style: TextStyle(color: Color(0xFFe0e1dd)),
                    )
                  ]),
                ]),
              ]),
        ),
        Container(
          padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
          child: Center(
              child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              onPrimary: Colors.white,
              primary: Color(0xFF748ba5),
            ),
            child: Text('Add'),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => HomeLab7()),
              );
            },
          )),
        ),
      ])),
    );
  }
}
