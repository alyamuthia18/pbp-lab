import 'package:flutter/material.dart';
import 'package:lab_7/main_drawer.dart';
import 'package:lab_7/pages/form.dart';

class HomeLab7 extends StatelessWidget {
  const HomeLab7({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Progress Tracker',
          style: TextStyle(color: Color(0xFFe0e1dd)),
        ),
        backgroundColor: Color(0xFF343434),
      ),
      drawer: MainDrawer(),
      body: FormPage(),
    );
  }
}
