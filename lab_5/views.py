from django.http import response
from django.shortcuts import render
from .models import Note

def index(request):
    notes = Note.objects.all().values()
    response = {'notes' : notes}
    return render(request, 'lab2.html', response)

