from django import forms
from django.db.models import fields
from .models import Note

class NoteForm(forms.ModelForm):
    class Meta: 
        model = Note
        fields = ['to', 'sender_info', 'title', 'message']

    to = forms.CharField(label='To',required=True, widget=forms.TextInput(attrs={'type' : 'text', 'placeholder' : 'Nama Penerima'}))
    sender_info = forms.CharField(label='Sender Info',required=True, widget=forms.TextInput(attrs={'type' : 'text', 'placeholder' : 'Nama Pengirim'}))
    title = forms.CharField(label='Title',required=True, widget=forms.TextInput(attrs={'type' : 'text', 'placeholder' : 'Judul Pesan'}))
    message = forms.CharField(label='Message',required=True, widget=forms.TextInput(attrs={'type' : 'text', 'placeholder' : 'Pesan untuk Penerima'}))