from django.shortcuts import render
from django.http import response
from lab_4.forms import NoteForm
from .models import Note
from django.http import HttpResponseRedirect

# Create your views here.

def index(request):
    notes = Note.objects.all().values()
    response = {'notes' : notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    form = NoteForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/lab-4')
    context = {'form' : form}
    return render(request, 'lab4_form.html', context)

def note_list(request):
    notes = Note.objects.all().values()
    response = {'notes' : notes}
    return render(request, 'lab4_note_list.html', response)
