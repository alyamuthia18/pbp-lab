from django.db import models
from django.db.models.fields import CharField

# Create your models here.

class Note(models.Model):
    to = models.CharField(max_length=100)
    sender_info = models.CharField(max_length=100)
    title = models.CharField(max_length=100)
    message = models.CharField(max_length=100)
