1. Apakah perbedaan antara JSON dan XML?
## JSON ##
- JSON (JavaScript Object Notation) merupakan format data utama pada JavaScript. Meskipun dapat menggunakan format data lainnya, penggunakan JSON pada JavaScript dianggap lebih mudah dan tidak membutuhkan effort lebih untuk menjalankannya, karena JSON sudah terintegrasi dan sangat tepat untuk digunakan pada JavaScript.
- Selain pada JavaScript, JSON juga dapat digunakan pada berbagai bahasa pemrograman
- Diciptakan untuk digunakan dalam pertukaran data dan menyimpan informasi
- Dibandingkan dengan XML, JSON lebih sederhana, mudah dibaca, dan diterjemahkan oleh manusia
- Objek pada JSON memiliki tipe
- Mendukung beberapa tipe data seperti string, number, array, dan Boolean
- Tampilan syntax nya dapat diubah
- Tidak dapat membuat komentar 
- Hanya mendukung UTF-8 encoding
- Didukung oleh hampir seluruh browser

## XML ##
- XMl (Extensible Markup Language) merupakan sebuah markup language yang berfungsi untuk menyimpan dan mengirimkan informasi dengan cara memasukkan informasi tersebut ke dalam sebuah tag.
- Datanya tidak memiliki tipe, keseluruhannya merupakan string
- Tampilannya bisa diubah
- Dapat menuliskan komentar
- Lebih aman dibandingkan dengan JSON
- Mendukung beberapa macam encoding format
- Karena menggunakan tag, XML lebih mudah diterjemahkan oleh mesin dibanding dengan JSON (lebih machine readable)
- Jumlah browser yang mendukung XML lebih sedikit dibanding dengan JSON

2. Apakah perbedaan antara HTML dan XML?
## HTML ##
- HyperText Markup Language
- Menampilkan data dan mendeskripsikan struktur dari sebuah halaman web
- Tidak case sensitive
- Tags sudah didefinisikan
- Tidak harus memiliki closing tags
- Default nya static

## XML ##
- Extensible Markup Language
- Menyimpan dan mengirimkan data.
- case sensitive
- Tags dibuat sesuai dengan kebutuhan programmer
- Harus menggunakan closing tags
- Defaultnya dynamic

Source :
Berga, Mariana.(2021, January 28). JSON VS. XML: WHICH ONE IS BETTER?. Retrieved from : https://www.imaginarycloud.com/blog/json-vs-xml/
Martin, Matthew.(2021, August 27). Difference between XML and HTML. Retrieved from : https://www.guru99.com/xml-vs-html-difference.html
Vats, Rohan.(2021, January 4). HTML Vs XML : Difference Between HTML and XML [2021]. Retrieved from : https://www.upgrad.com/blog/html-vs-xml/
Vivekkhothari.(2019, February 19). Difference between JSON and XML. Retrieved from : (https://www.geeksforgeeks.org/difference-between-json-and-xml/ 
Walker, Alyssa.(2021, August 27). JSON vs XML: What's the DIfference?. Retrieved from : https://www.guru99.com/json-vs-xml-difference.html